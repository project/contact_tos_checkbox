<?php

namespace Drupal\contact_tos_checkbox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ContactTosCheckboxSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'contact_tos_checkbox_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['contact_tos_checkbox.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, ?FormStateInterface $formState): array {
    $config = $this->config('contact_tos_checkbox.settings');

    $form['feedback_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable checkbox in the default feedback form'),
      '#default_value' => $config->get('feedback.enabled'),
    ];

    $form['feedback_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('feedback.label'),
      '#required' => TRUE,
    ];

    $form['feedback_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('feedback.description'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState): void {
    $this->config('contact_tos_checkbox.settings')
      ->set('feedback.enabled', $formState->getValue('feedback_enabled'))
      ->set('feedback.label', $formState->getValue('feedback_label'))
      ->set('feedback.description', $formState->getValue('feedback_description'))
      ->save();

    parent::submitForm($form, $formState);
  }

}
